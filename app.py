from flask import Flask, request, make_response, jsonify
import jwt
import datetime
from functools import wraps

app = Flask(__name__)
app.config['SECRET_KEY'] = 'estaesmifrasesecreta'

def token_requerido(f):
   @wraps(f)
   def decorated(*args, **kwargs):
      token = request.args.get('token')
      
      if not token:
         return jsonify({'mensaje' : 'No existe el token'}), 401
      
      try:
         data = jwt.decode(token, app.config['SECRET_KEY'])
      except:
         return jsonify({'mensaje' : 'Token es invalido'}), 401

      return f(*args, **kwargs)
   return decorated

@app.route('/desprotegido')
def desprotegido():
   return jsonify({'mensaje' : 'Eres millonario, en espiritu'})

@app.route('/protegido')
@token_requerido
def protegido():
   return jsonify({'mensaje' : 'Eres millonario, mira tu cuenta bancaria'})

@app.route('/login')
def login():
   auth = request.authorization

   if auth and auth.password == 'secreto':
      token = jwt.encode({'user' : auth.username, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(seconds=30)}, app.config['SECRET_KEY'])
      return jsonify({'token' : token.decode('UTF-8')})
      
  
   return make_response('No se pudo verificar', 401, {'WWW-Authenticate' : 'Basic realm="Login Requerido"'})

if __name__ == '__main__':
   app.run(debug=True) 
